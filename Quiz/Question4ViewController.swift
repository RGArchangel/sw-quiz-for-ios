//
//  Question4ViewController.swift
//  Quiz
//
//  Created by Archangel on 10.02.2019.
//  Copyright © 2019 Archangel. All rights reserved.
//

import Foundation
import UIKit

class Question4ViewController: UIViewController {
    
    @IBAction func onButtonTouch(sender: UIButton)
    {
        let back: UIImage = UIImage(named: "button-ON")!
        sender.setBackgroundImage(back, for: UIControl.State.normal)
        
        let s : String = sender.title(for: UIControl.State.normal)!
        if s == "Мустафар"
        {
            FinalViewController.numOfCorrectAnswers += 1
        }
        
        performSegue(withIdentifier: "goToQuestion5", sender: self)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
}
