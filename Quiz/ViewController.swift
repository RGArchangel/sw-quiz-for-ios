//
//  ViewController.swift
//  Quiz
//
//  Created by Archangel on 07.02.2019.
//  Copyright © 2019 Archangel. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //Start
    @IBAction func beginButtonTouch(sender: UIButton)
    {
        FinalViewController.numOfCorrectAnswers = 0
        performSegue(withIdentifier: "goToQuestion1", sender: self)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
}

