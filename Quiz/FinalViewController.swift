//
//  FinalViewController.swift
//  Quiz
//
//  Created by Archangel on 10.02.2019.
//  Copyright © 2019 Archangel. All rights reserved.
//

import Foundation
import UIKit

class FinalViewController: UIViewController {
    
    @IBOutlet var score: UITextField!
    @IBOutlet var result: UITextField!
    
    static var numOfCorrectAnswers : Int!
    
    @IBAction func onButtonTouch(sender: UIButton)
    {
        performSegue(withIdentifier: "goBack", sender: self)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        score.text = String(FinalViewController.numOfCorrectAnswers) + "/5"
        
        switch FinalViewController.numOfCorrectAnswers{
        case 0:
            result.text = "Дроид-астромех"
        case 1:
            result.text = "Ученик"
        case 2:
            result.text = "Падаван"
        case 3:
            result.text = "Рыцарь-Джедай"
        case 4:
            result.text = "Джедай-консул"
        case 5:
            result.text = "Магистр-Джедай"
        default:
            result.text = "WTF"
        }
    }
}
